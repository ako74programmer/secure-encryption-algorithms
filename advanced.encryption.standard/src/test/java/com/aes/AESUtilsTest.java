package com.aes;

import org.junit.jupiter.api.Test;

import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AESUtilsTest {

    @Test
    void shouldDecryptedStringBeEqualToOriginalString() throws NoSuchAlgorithmException {
        byte[] bytes = AESUtils.generateKey(128).getEncoded();
        final String secretKey = new String(bytes, StandardCharsets.UTF_8); //"JHKLXABYZC!!!!";

        String originalString = "Java Developer";
        String encryptedString = AESUtils.encrypt(originalString, secretKey) ;
        String decryptedString = AESUtils.decrypt(encryptedString, secretKey) ;

        assertEquals(originalString, decryptedString);
    }
}