package com.console;

import com.aes.AESUtils;

import java.io.Console;
import java.util.logging.Logger;

public class PasswordEncoder {
    private static final Logger log = Logger.getLogger(PasswordEncoder.class.getName());

    public void encryptPassword() {
        Console console = System.console();
        if (console == null) {
            log.info("Couldn't get Console instance");
            System.exit(0);
        }

        console.printf("Enter enter your secret key: ");
        String secretKey = console.readLine();

        char[] passwordArray = console.readPassword("Enter your secret password: ");
        String password = new String(passwordArray);
        console.printf("Password entered was: %s%n", password);

        String encryptedPassword = AESUtils.encrypt(password, secretKey) ;
        console.printf("Encrypted password is: %s%n",  encryptedPassword);

        String decryptedPassword = AESUtils.decrypt(encryptedPassword, secretKey) ;
        console.printf("Decrypted password is: %s%n",  decryptedPassword);
    }

    public static void main(String[] args) {

        new PasswordEncoder().encryptPassword();
    }
}
