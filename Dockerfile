FROM openjdk:11
COPY . /tmp
WORKDIR /tmp
CMD ["java", "-jar", "password-console.jar"]
